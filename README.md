<h1 align="center">Spigot/Bukkit System Plugin - System</h1>
<p align="center">
    <a href="https://github.com/CrazyCraftLand/PermissionSystem-Spigot">
        <img src="https://img.shields.io/badge/version-1.0-blue.svg" alt="Changelog" />
    </a>
    <a href="https://github.com/CrazyCraftLand/PermissionSystem-Spigot">
        <img src="https://img.shields.io/badge/release-stable-orange.svg" alt="development" />
    </a>
    <a href="https://creativecommons.org/licenses/by-sa/4.0/">
        <img src="https://img.shields.io/badge/license-CC--BY--SA-4CB697.svg" alt="License" />
    </a>
</p>

