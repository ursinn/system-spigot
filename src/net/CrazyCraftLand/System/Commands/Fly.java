package net.CrazyCraftLand.System.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.CrazyCraftLand.System.Main;

/**
 * 
 * System
 * @author ursinn (Ursin Filli)
 * @Copyright (c) 2017 Ursin Filli
 * @license CC-BY-SA 4.0 International
 *
 */
public class Fly implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("fly")) {
			if (!sender.hasPermission("ccl.System.command.fly") && !sender.hasPermission("ccl.System.command.*")) {
				sender.sendMessage(Main.getInstance().NoPerms);
				return true;
			}
			
			if (args.length == 0) {
				if (sender instanceof Player) {
					Player p = (Player) sender;
					if (p.getAllowFlight()) {
						p.setAllowFlight(false);
						sender.sendMessage(Main.getInstance().Prefix + "�cFliegen Deaktiviert!");
						return true;
					} else {
						p.setAllowFlight(true);
						sender.sendMessage(Main.getInstance().Prefix + "�aFliegen Aktiviert!");
						return true;
					}
				} else {
					sender.sendMessage(Main.getInstance().Prefix + "�c/Fly <Name>");
					return true;
				}
			} else if (args.length == 1) {
				if (!sender.hasPermission("ccl.System.command.fly.other") && !sender.hasPermission("ccl.System.command.*")) {
					sender.sendMessage(Main.getInstance().NoPerms);
					return true;
				}
				
				Player t = Bukkit.getPlayer(args[0]);
				if (t == null) {
					sender.sendMessage(Main.getInstance().Prefix + "�cDer Spieler ist Offline!");
					return true;
				}
				
				if (t.getAllowFlight()) {
					t.setAllowFlight(false);
					t.sendMessage(Main.getInstance().Prefix + "�cFliegen Deaktiviert!");
					sender.sendMessage(
							Main.getInstance().Prefix + "�cFliegen von �6" + args[0] + "�c Deaktiviert!");
					return true;
				} else {
					t.setAllowFlight(true);
					t.sendMessage(Main.getInstance().Prefix + "�aFliegen Aktiviert!");
					sender.sendMessage(
							Main.getInstance().Prefix + "�aFliegen von �6" + args[0] + "�a Aktiviert!");
					return true;
				}
			} else {
				sender.sendMessage(Main.getInstance().Prefix + "�c/Fly (Name)");
				return true;
			}
		}
		return true;
	}
	
}
