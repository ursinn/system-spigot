package net.CrazyCraftLand.System.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.CrazyCraftLand.System.Main;

/**
 * 
 * System
 * @author ursinn (Ursin Filli)
 * @Copyright (c) 2017 Ursin Filli
 * @license CC-BY-SA 4.0 International
 *
 */
public class Time implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("day") || cmd.getName().equalsIgnoreCase("tag")) {
			if (sender instanceof Player) {
				Player p = (Player) sender;
				if (!p.hasPermission("ccl.System.command.day") && !sender.hasPermission("ccl.System.command.*")) {
					p.sendMessage(Main.getInstance().NoPerms);
					return true;
				}
				
				p.getWorld().setTime(0L);
				p.sendMessage(Main.getInstance().Prefix + "�aEs ist nun Tag!");
				return true;
			}
			sender.sendMessage(Main.getInstance().NoConsole);
			return true;
		}
		
		if (cmd.getName().equalsIgnoreCase("night") || cmd.getName().equalsIgnoreCase("nacht")) {
			if (sender instanceof Player) {
				Player p = (Player) sender;
				if (!p.hasPermission("ccl.System.command.night") && !sender.hasPermission("ccl.System.command.*")) {
					p.sendMessage(Main.getInstance().NoPerms);
					return true;
				}
				
				p.getWorld().setTime(20000L);
				p.sendMessage(Main.getInstance().Prefix + "�aEs ist nun Nacht!");
				return true;
			}
			sender.sendMessage(Main.getInstance().NoConsole);
			return true;
		}
		return true;
	}

}
