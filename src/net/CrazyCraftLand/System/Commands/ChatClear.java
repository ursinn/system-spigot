package net.CrazyCraftLand.System.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.CrazyCraftLand.System.Main;

/**
 * 
 * System
 * @author ursinn (Ursin Filli)
 * @Copyright (c) 2017 Ursin Filli
 * @license CC-BY-SA 4.0 International
 *
 */
public class ChatClear implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("cc") || cmd.getName().equalsIgnoreCase("ChatClear")) {
			if (!sender.hasPermission("ccl.System.command.ChatClear") && !sender.hasPermission("ccl.System.command.*")) {
				sender.sendMessage(Main.getInstance().NoPerms);
				return true;
			}

			for(int i = 0; i < 256; i++) {
				sendMessageToAllPlayers(" ");
			}

			if (sender instanceof Player) {
				Player p = (Player)sender;
				Bukkit.broadcastMessage("�cDer Chat wurde von �c" + p.getDisplayName() + " �cGecleart");
			} else {
				Bukkit.broadcastMessage("�cDer Chat wurde von �c" + sender.getName() + " �cGecleart");
			}
		}
		return true;
	}

	/**
	 * @param string
	 */
	private void sendMessageToAllPlayers(String string) {
		for(Player all : Bukkit.getOnlinePlayers()) {
			all.sendMessage(string);
		}
	}
	
}
