package net.CrazyCraftLand.System.Commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.CrazyCraftLand.System.Main;

/**
 * 
 * System
 * @author ursinn (Ursin Filli)
 * @Copyright (c) 2017 Ursin Filli
 * @license CC-BY-SA 4.0 International
 *
 */
public class CommandSpy implements CommandExecutor {

	public static List<Player> PlayersEnable = new ArrayList<>();

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("commandspy")) {
			if (sender instanceof Player) {
				Player p = (Player) sender;
				if (!p.hasPermission("ccl.System.command.CommandSpy") && !sender.hasPermission("ccl.System.command.*")) {
					p.sendMessage(Main.getInstance().NoPerms);
					return true;
				}
				
				if (PlayersEnable.contains(p)) {
					PlayersEnable.remove(p);
					sender.sendMessage("§cCommandSpy Disabled");
				} else {
					PlayersEnable.add(p);
					sender.sendMessage("§aCommandSpy Enabled");
				}
				return true;
			}
			sender.sendMessage(Main.getInstance().NoConsole);
			return true;
		}
		return true;
	}

}
