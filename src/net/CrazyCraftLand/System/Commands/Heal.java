package net.CrazyCraftLand.System.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.CrazyCraftLand.System.Main;

/**
 * 
 * System
 * @author ursinn (Ursin Filli)
 * @Copyright (c) 2017 Ursin Filli
 * @license CC-BY-SA 4.0 International
 *
 */
public class Heal implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("heal")) {
			if (args.length == 0) {
				if (!sender.hasPermission("ccl.System.command.heal") && !sender.hasPermission("ccl.System.command.*")) {
					sender.sendMessage(Main.getInstance().NoPerms);
					return true;
				}
				
				if (sender instanceof Player) {
					Player p = (Player) sender;
					p.setHealth(20D);
					p.setFoodLevel(20);
					p.sendMessage(Main.getInstance().Prefix + "�aDu hast  dich Geheilt!");
					return true;
				}
				sender.sendMessage(Main.getInstance().Prefix + "�c/heal <Name>");
			}
			
			if (args.length == 1) {
				if (!sender.hasPermission("ccl.System.command.heal.other") && !sender.hasPermission("ccl.System.command.*")) {
					sender.sendMessage(Main.getInstance().NoPerms);
					return true;
				}
				
				Player t = Bukkit.getPlayer(args[0]);
				if (t != null) {
					t.setHealth(20D);
					t.setFoodLevel(20);
					t.sendMessage(Main.getInstance().Prefix + "�aDu wurdest Geheilt!");
					sender.sendMessage(Main.getInstance().Prefix + "�aDu hast �6" + args[0] + " �a Geheilt!");
					return true;
				}
				sender.sendMessage(Main.getInstance().Prefix + "�cDer Spieler ist Offline");
				return true;
			}
			sender.sendMessage(Main.getInstance().Prefix + "�c/heal (Name)");
			return true;
		}
		
		if (cmd.getName().equalsIgnoreCase("feed")) {
			if (args.length == 0) {
				if (!sender.hasPermission("ccl.System.command.feed") && !sender.hasPermission("ccl.System.command.*")) {
					sender.sendMessage(Main.getInstance().NoPerms);
					return true;
				}
				
				if (sender instanceof Player) {
					Player p = (Player) sender;
					p.setHealth(20D);
					p.setFoodLevel(20);
					p.sendMessage(Main.getInstance().Prefix + "�aDu hast deine Hungerleiste aufgef�hlt!");
					return true;
				}
				sender.sendMessage(Main.getInstance().Prefix + "�c/feed <Name>");
			}
			
			if (args.length == 1) {
				if (!sender.hasPermission("ccl.System.command.feed.other") && !sender.hasPermission("ccl.System.command.*")) {
					sender.sendMessage(Main.getInstance().NoPerms);
					return true;
				}
				
				Player t = Bukkit.getPlayer(args[0]);
				if (t != null) {
					t.setHealth(20D);
					t.setFoodLevel(20);
					t.sendMessage(Main.getInstance().Prefix + "�aDeine Hungerleiste wurde aufgef�hlt!");
					sender.sendMessage(Main.getInstance().Prefix + "�aDu hast die Hungerleiste von �6" + args[0]
							+ " �a Aufgef�hlt!");
					return true;
				}
				sender.sendMessage(Main.getInstance().Prefix + "�cDer Spieler ist Offline");
				return true;
			}
			sender.sendMessage(Main.getInstance().Prefix + "�c/feed (Name)");
			return true;
		}
		return true;
	}

}
