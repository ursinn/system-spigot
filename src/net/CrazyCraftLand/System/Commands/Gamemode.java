package net.CrazyCraftLand.System.Commands;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.CrazyCraftLand.System.Main;

/**
 * 
 * System
 * @author ursinn (Ursin Filli)
 * @Copyright (c) 2017 Ursin Filli
 * @license CC-BY-SA 4.0 International
 *
 */
public class Gamemode implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("gm")) {
			if (!sender.hasPermission("ccl.System.command.gm") && !sender.hasPermission("ccl.System.command.*")) {
				sender.sendMessage(Main.getInstance().NoPerms);
				return true;
			}

			if (args.length == 1) {
				if (sender instanceof Player) {
					Player p = (Player) sender;
					if (args[0].equalsIgnoreCase("0")) {
						// Gamemode: 0 (Survival)
						p.setGameMode(GameMode.SURVIVAL);
						p.sendMessage(Main.getInstance().Prefix + "�aDu bist nun in gm 0!");
						return true;
					}
					if (args[0].equalsIgnoreCase("1")) {
						// Gamemode: 1 (Creative)
						p.setGameMode(GameMode.CREATIVE);
						p.sendMessage(Main.getInstance().Prefix + "�aDu bist nun in gm 1!");
						return true;
					}
					if (args[0].equalsIgnoreCase("2")) {
						// Gamemode: 2 (Adventure)
						p.setGameMode(GameMode.ADVENTURE);
						p.sendMessage(Main.getInstance().Prefix + "�aDu bist nun in gm 2!");
						return true;
					}
					if (args[0].equalsIgnoreCase("3")) {
						// Gamemode: 3 (Spectator)
						p.setGameMode(GameMode.SPECTATOR);
						p.sendMessage(Main.getInstance().Prefix + "�aDu bist nun in gm 3!");
						return true;
					}
					sender.sendMessage(Main.getInstance().Prefix + "�c/gm <0/1/2/3> (Name)");
					return true;
				}
				sender.sendMessage(Main.getInstance().Prefix + "�c/gm <0/1/2/3> <Name>");
				return true;
			} else if (args.length == 2) {
				Player p = Bukkit.getPlayer(args[1]);
				if (!sender.hasPermission("ccl.System.command.gm.other") && !sender.hasPermission("ccl.System.command.*")) {
					sender.sendMessage(Main.getInstance().NoPerms);
					return true;
				}
				
				if (p != null) {
					if (args[0].equalsIgnoreCase("0")) {
						// Gamemode: 0 (Survival)
						p.setGameMode(GameMode.SURVIVAL);
						p.sendMessage(Main.getInstance().Prefix + "�aDu bist nun in gm 0!");
						sender.sendMessage(
								Main.getInstance().Prefix + "�aDu hast �c" + args[1] + "�a GM 0 gesetzt!");
						return true;
					}
					if (args[0].equalsIgnoreCase("1")) {
						// Gamemode: 1 (Creative)
						p.setGameMode(GameMode.CREATIVE);
						p.sendMessage(Main.getInstance().Prefix + "�aDu bist nun in gm 1!");
						sender.sendMessage(
								Main.getInstance().Prefix + "�aDu hast �c" + args[1] + "�a GM 1 gesetzt!");
						return true;
					}
					if (args[0].equalsIgnoreCase("2")) {
						// Gamemode: 2 (Adventure)
						p.setGameMode(GameMode.ADVENTURE);
						p.sendMessage(Main.getInstance().Prefix + "�aDu bist nun in gm 2!");
						sender.sendMessage(
								Main.getInstance().Prefix + "�aDu hast �c" + args[1] + "�a GM 2 gesetzt!");
						return true;
					}
					if (args[0].equalsIgnoreCase("3")) {
						// Gamemode: 3 (Spectator)
						p.setGameMode(GameMode.SPECTATOR);
						p.sendMessage(Main.getInstance().Prefix + "�aDu bist nun in gm 3!");
						sender.sendMessage(
								Main.getInstance().Prefix + "�aDu hast �c" + args[1] + "�a GM 3 gesetzt!");
						return true;
					}
					sender.sendMessage(Main.getInstance().Prefix + "�c/gm <0/1/2/3> (Name)");
					return true;
				}
				sender.sendMessage(Main.getInstance().Prefix + "�cFehler: Der Spieler �a" + args[1]
						+ " �c Ist nicht Online!");
				return true;
			}
			sender.sendMessage(Main.getInstance().Prefix + "�c/gm <0/1/2/3> (Name)");
			return true;
		}
		
		// GMC
		if (cmd.getName().equalsIgnoreCase("gmc")) {
			if (!sender.hasPermission("ccl.System.command.gmc") && !sender.hasPermission("ccl.System.command.*")) {
				sender.sendMessage(Main.getInstance().NoPerms);
				return true;
			}
			
			if (args.length == 0) {
				if (!(sender instanceof Player)) {
					sender.sendMessage(Main.getInstance().Prefix + "�c/gmc <Name>");
					return true;
				}
				
				// Gamemode: 1 (Creative)
				Player p = (Player) sender;
				p.setGameMode(GameMode.CREATIVE);
				sender.sendMessage(Main.getInstance().Prefix + "�aDu bist nun in gm 1!");
				return true;
			}
			
			if (args.length == 1) {
				Player t = Bukkit.getPlayer(args[0]);
				if (!sender.hasPermission("ccl.System.command.gmc.other") && !sender.hasPermission("ccl.System.command.*")) {
					sender.sendMessage(Main.getInstance().NoPerms);
					return true;
				}
				
				if (t == null) {
					sender.sendMessage(Main.getInstance().Prefix + "�cFehler: Der Spieler �a" + args[1]
							+ " �c Ist nicht Online!");
					return true;
				}
				
				// Gamemode: 1 (Creative)
				t.setGameMode(GameMode.CREATIVE);
				t.sendMessage(Main.getInstance().Prefix + "�aDu bist nun in gm 1!");
				sender.sendMessage(Main.getInstance().Prefix + "�aDu hast �c" + args[1] + "�a GM 1 gesetzt!");
				return true;
			}
			sender.sendMessage(Main.getInstance().Prefix + "�c/gmc (Name)");
			return true;
		}
		
		// GMS
		if (cmd.getName().equalsIgnoreCase("gms")) {
			if (!sender.hasPermission("ccl.System.command.gms") && !sender.hasPermission("ccl.System.command.*")) {
				sender.sendMessage(Main.getInstance().NoPerms);
				return true;
			}
			
			if (args.length == 0) {
				if (!(sender instanceof Player)) {
					sender.sendMessage(Main.getInstance().Prefix + "�c/gms <Name>");
					return true;
				}
				
				// Gamemode: 0 (Survival)
				Player p = (Player) sender;
				p.setGameMode(GameMode.SURVIVAL);
				sender.sendMessage(Main.getInstance().Prefix + "�aDu bist nun in gm 0!");
				return true;
			}
			
			if (args.length == 1) {
				Player t = Bukkit.getPlayer(args[0]);
				if (!sender.hasPermission("ccl.System.command.gms.other") && !sender.hasPermission("ccl.System.command.*")) {
					sender.sendMessage(Main.getInstance().NoPerms);
					return true;
				}
				
				if (t == null) {
					sender.sendMessage(Main.getInstance().Prefix + "�cFehler: Der Spieler �a" + args[1]
							+ " �c Ist nicht Online!");
					return true;
				}
				
				// Gamemode: 0 (Survival)
				t.setGameMode(GameMode.SURVIVAL);
				t.sendMessage(Main.getInstance().Prefix + "�aDu bist nun in gm 0!");
				sender.sendMessage(Main.getInstance().Prefix + "�aDu hast �c" + args[1] + "�a GM 0 gesetzt!");
				return true;
			}
			sender.sendMessage(Main.getInstance().Prefix + "�c/gms (Name)");
			return true;
		}
		
		// GMA
		if (cmd.getName().equalsIgnoreCase("gma")) {
			if (!sender.hasPermission("ccl.System.command.gma") && !sender.hasPermission("ccl.System.command.*")) {
				sender.sendMessage(Main.getInstance().NoPerms);
				return true;
			}
			
			if (args.length == 0) {
				if (!(sender instanceof Player)) {
					sender.sendMessage(Main.getInstance().Prefix + "�c/gma <Name>");
					return true;
				}
				
				// Gamemode: 2 (Adventure)
				Player p = (Player) sender;
				p.setGameMode(GameMode.ADVENTURE);
				sender.sendMessage(Main.getInstance().Prefix + "�aDu bist nun in gm 2!");
				return true;
			}
			
			if (args.length == 1) {
				Player t = Bukkit.getPlayer(args[0]);
				if (!sender.hasPermission("ccl.System.command.gma.other") && !sender.hasPermission("ccl.System.command.*")) {
					sender.sendMessage(Main.getInstance().NoPerms);
					return true;
				}
				
				if (t == null) {
					sender.sendMessage(Main.getInstance().Prefix + "�cFehler: Der Spieler �a" + args[1]
							+ " �c Ist nicht Online!");
					return true;
				}
				
				// Gamemode: 2 (Adventure)
				t.setGameMode(GameMode.SURVIVAL);
				t.sendMessage(Main.getInstance().Prefix + "�aDu bist nun in gm 2!");
				sender.sendMessage(Main.getInstance().Prefix + "�aDu hast �c" + args[1] + "�a GM 2 gesetzt!");
				return true;
			}
			sender.sendMessage(Main.getInstance().Prefix + "�c/gma (Name)");
			return true;
		}
		return true;
	}

}
