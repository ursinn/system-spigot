package net.CrazyCraftLand.System.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.CrazyCraftLand.System.Main;

/**
 * 
 * System
 * @author ursinn (Ursin Filli)
 * @Copyright (c) 2017 Ursin Filli
 * @license CC-BY-SA 4.0 International
 *
 */
public class Server implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("ts") || cmd.getName().equalsIgnoreCase("teamspeak")) {
			sender.sendMessage("�6--------------[�a " + Main.getInstance().ServerName + " TS3 �6]--------------");
			sender.sendMessage("");
			sender.sendMessage("�a1. �3�ffne TeamSpeak. ");
			sender.sendMessage("�a2. �3Klicke auf Verbinden.");
			sender.sendMessage("�a3. �3Gebe die IP ein");
			sender.sendMessage("�a4. �c" + Main.getInstance().Ts3_IP);
			sender.sendMessage("�a5. �3Klicke auf Verbinden...");
			sender.sendMessage("�a6. �3Und Spreche mit deinen Freunden.");
			sender.sendMessage("");
			sender.sendMessage("�6---------------[�a " + Main.getInstance().ServerName + " TS3 �6]---------------");
		}
		
		if (cmd.getName().equalsIgnoreCase("Forum")) {
			sender.sendMessage("�6---------------[�a " + Main.getInstance().ServerName + " Forum �6]---------------");
			sender.sendMessage("");
			sender.sendMessage("�a1. �3Du willst uns im Web besuchen?");
			sender.sendMessage("�a2. �3Dort kannst du News, das Team uvm. finden.");
			sender.sendMessage("�a3. �3Klicke auf den Link um auf unser Forum zu kommen");
			sender.sendMessage("�a4. �3Link");
			sender.sendMessage("�a5. �c" + Main.getInstance().Web_Forum);
			sender.sendMessage("");
			sender.sendMessage("�6---------------[�a " + Main.getInstance().ServerName + " Forum �6]---------------");
		}
		
		if (cmd.getName().equalsIgnoreCase("Webseite")) {
			sender.sendMessage("�6---------------[�a " + Main.getInstance().ServerName + " Website �6]---------------");
			sender.sendMessage("");
			sender.sendMessage("�a1. �3Du willst uns im Web besuchen?");
			sender.sendMessage("�a2. �3Dort kannst du News, das Team uvm. finden.");
			sender.sendMessage("�a3. �3Klicke auf den Link um auf unsere Seite zu kommen");
			sender.sendMessage("�a4. �3Link");
			sender.sendMessage("�a5. �c" + Main.getInstance().Web_Home);
			sender.sendMessage("");
			sender.sendMessage("�6---------------[�a " + Main.getInstance().ServerName + " Website �6]---------------");
		}
		
		if (cmd.getName().equalsIgnoreCase("invsee")) {
			if (sender instanceof Player) {
				Player p = (Player) sender;
				if (!p.hasPermission("ccl.System.command.invsee") && !sender.hasPermission("ccl.System.command.*")) {
					p.sendMessage(Main.getInstance().NoPerms);
					return true;
				}
				
				if (args.length == 1) {
					Player t = Bukkit.getPlayer(args[0]);
					if (t != null) {
						p.openInventory(t.getInventory());
						return true;
					} else {
						sender.sendMessage(Main.getInstance().Prefix + "�cDer Spieler ist Offline");
						return true;
					}
				} else {
					sender.sendMessage(Main.getInstance().Prefix + "�c/invsee <Name>");
					return true;
				}
			} else {
				sender.sendMessage(Main.getInstance().NoConsole);
				return true;
			}
		}
		return true;
	}
	
}
