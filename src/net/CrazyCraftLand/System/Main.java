package net.CrazyCraftLand.System;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import net.CrazyCraftLand.System.Commands.ChatClear;
import net.CrazyCraftLand.System.Commands.CommandSpy;
import net.CrazyCraftLand.System.Commands.Fly;
import net.CrazyCraftLand.System.Commands.Gamemode;
import net.CrazyCraftLand.System.Commands.Heal;
import net.CrazyCraftLand.System.Commands.Server;
import net.CrazyCraftLand.System.Commands.Time;
import net.CrazyCraftLand.System.Listener.Command;
import net.CrazyCraftLand.System.Listener.Sign;

/**
 * 
 * System
 * @author ursinn (Ursin Filli)
 * @Copyright (c) 2017 Ursin Filli
 * @license CC-BY-SA 4.0 International
 *
 */
public class Main extends JavaPlugin {

	public static Main instance;

	public String Prefix = "�7[�aSystem�7]�f ";
	public String NoPerms;
	public String NoConsole;
	
	public String ServerName;
	public String Ts3_IP;
	public String Web_Forum;
	public String Web_Home;
	
	public void onEnable() {
		instance = this;
		Bukkit.getConsoleSender().sendMessage(Prefix + "�aEnabled!");
		registerCommands();
		registerEvents();
	}

	public void onDisable() {
		Bukkit.getConsoleSender().sendMessage(Prefix + "�cDisabled!");
	}
	
	private void registerEvents() {
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new Command(), this);
		pm.registerEvents(new Sign(), this);
	}

	private void registerCommands() {
		ChatClear ChatClearCMD = new ChatClear();
		Fly FlyCMD = new Fly();
		Time TimeCMD = new Time();
		Heal HealCMD = new Heal();
		CommandSpy SPCMD = new CommandSpy();
		Server ServerCMD = new Server();
		Gamemode GamemodeCMD = new Gamemode();
		/* ChatClear */
		getCommand("cc").setExecutor(ChatClearCMD);
		getCommand("ChatClear").setExecutor(ChatClearCMD);
		/* Fly */
		getCommand("Fly").setExecutor(FlyCMD);
		/* Time */
		getCommand("night").setExecutor(TimeCMD);
		getCommand("nacht").setExecutor(TimeCMD);
		getCommand("day").setExecutor(TimeCMD);
		getCommand("tag").setExecutor(TimeCMD);
		/* Heal */
		getCommand("heal").setExecutor(HealCMD);
		getCommand("feed").setExecutor(HealCMD);
		/* CommandSpy */
		getCommand("commandspy").setExecutor(SPCMD);
		/* Server */
		getCommand("ts").setExecutor(ServerCMD);
		getCommand("teamspeak").setExecutor(ServerCMD);
		getCommand("forum").setExecutor(ServerCMD);
		getCommand("webseite").setExecutor(ServerCMD);
		getCommand("invsee").setExecutor(ServerCMD);
		/* Gamemode */
		getCommand("gm").setExecutor(GamemodeCMD);
		getCommand("gmc").setExecutor(GamemodeCMD);
		getCommand("gms").setExecutor(GamemodeCMD);
		getCommand("gma").setExecutor(GamemodeCMD);
	}

	public static Main getInstance() {
		return instance;
	}
}
