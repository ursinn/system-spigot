package net.CrazyCraftLand.System.Listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.help.HelpTopic;

import net.CrazyCraftLand.System.Main;
import net.CrazyCraftLand.System.Commands.CommandSpy;

/**
 * 
 * System
 * @author ursinn (Ursin Filli)
 * @Copyright (c) 2017 Ursin Filli
 * @license CC-BY-SA 4.0 International
 *
 */
public class Command implements Listener {

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onCheck(PlayerCommandPreprocessEvent e) {
		Player p = e.getPlayer();
		// /pl /? /plugin /version /ver /about
		if (e.getMessage().equalsIgnoreCase("/pl") || e.getMessage().equalsIgnoreCase("/?")
				|| e.getMessage().equalsIgnoreCase("/plugin") || e.getMessage().equalsIgnoreCase("/version") || e.getMessage().equalsIgnoreCase("/ver") || e.getMessage().equalsIgnoreCase("/about")) {
			if (!p.hasPermission("ccl.System.showinfos")) {
				p.sendMessage(Main.getInstance().NoPerms);
				e.setCancelled(true);
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onSend(PlayerCommandPreprocessEvent e) {
		Player p = e.getPlayer();
		if (!e.isCancelled()) {
			String msg = e.getMessage().split(" ")[0];
			HelpTopic topic = Bukkit.getServer().getHelpMap().getHelpTopic(msg);
			if (topic == null) {
				p.sendMessage(Main.getInstance().Prefix + "�4Unbekannter Befehl!");
				e.setCancelled(true);
			}
		}
		for (Player all : Bukkit.getOnlinePlayers()) {
			if (CommandSpy.PlayersEnable.contains(all)) {
				if (e.getPlayer() != all) {
					all.sendMessage("�cCommandSpy �7: �a[�6" + e.getPlayer().getName() + "�a] �c" + e.getMessage());
				}
			}
		}
	}
	
}
