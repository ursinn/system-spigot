package net.CrazyCraftLand.System.Listener;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

/**
 * 
 * System
 * @author ursinn (Ursin Filli)
 * @Copyright (c) 2017 Ursin Filli
 * @license CC-BY-SA 4.0 International
 *
 */
public class Sign implements Listener {

	@EventHandler
	public void OnFarbigeSignChange(SignChangeEvent e) {
		String Lineie0 = e.getLine(0);
		String Lineie1 = e.getLine(1);
		String Lineie2 = e.getLine(2);
		String Lineie3 = e.getLine(3);
		String trans0 = ChatColor.translateAlternateColorCodes('&', Lineie0);
		String trans1 = ChatColor.translateAlternateColorCodes('&', Lineie1);
		String trans2 = ChatColor.translateAlternateColorCodes('&', Lineie2);
		String trans3 = ChatColor.translateAlternateColorCodes('&', Lineie3);
		e.setLine(0, trans0);
		e.setLine(1, trans1);
		e.setLine(2, trans2);
		e.setLine(3, trans3);
	}
	
}
